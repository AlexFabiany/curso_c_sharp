﻿using System;
using System.Collections.Generic;

namespace CursoCSharp.OO {
	class Abstract {
		// Classe abstrata não pode ser instaciada, só herdada
		public abstract class Celular {
			// Método abstrato deve ser implementado, obrigatoriamente, pela classe que herdar
			public abstract string Assistente();

			public string Tocar() {
				return "Trim trim trim...";
			}
		}

		public class Samsung : Celular {
			// Implementação da classe abstrata usando override
			public override string Assistente() {
				return "Olá, meu nome é Bixby!";
			}
		}

		public class IPhone : Celular {
			public override string Assistente() {
				return "Olá, meu nome é Siri!";
			}
		}
		public static void Executar() {
			var celulares = new List<Celular> {
				new Samsung(),
				new IPhone()
			};

			foreach (var celular in celulares) {
				Console.WriteLine(celular.Assistente());
			}
		}
	}
}
