﻿using System;

namespace CursoCSharp.OO {
	public class Carro {
		protected readonly int VelocidadeMaxima;
		int VelocidadeAtual;

		public Carro(int velocidadeMaxima) {
			this.VelocidadeMaxima = velocidadeMaxima;
		}

		protected int AlterarVelocidade(int delta) {
			int novaVelocidade = this.VelocidadeAtual + delta;
			if (novaVelocidade < 0) {
				this.VelocidadeAtual = 0;
			} else if (novaVelocidade > this.VelocidadeMaxima) {
				this.VelocidadeAtual = this.VelocidadeMaxima;
			} else {
				this.VelocidadeAtual = novaVelocidade;
			}

			return this.VelocidadeAtual;
		}

		public virtual int Acelerar() {
			return AlterarVelocidade(5);
		}

		public int Frear() {
			return AlterarVelocidade(-5);
		}
	}

	public class Uno : Carro {
		public Uno() : base(200) {

		}
	}

	public class Ferrari : Carro {
		public Ferrari() : base(350) {

		}

		public override int Acelerar() { // Palavra chave "override"... Sobrescreve o método base. Requer "virtual" no mérodo base
			return AlterarVelocidade(15);
		}

		public new int Frear() { // Palavra chave "new"... Oculta o método base e cria um novo
			return AlterarVelocidade(-15);
		}
	}
	class Heranca {
		public static void Executar() {
			Uno uno = new Uno();
			Console.WriteLine("Uno...");
			Console.WriteLine($"Acelerando.. {uno.Acelerar()}Km/h...");
			Console.WriteLine($"Acelerando.. {uno.Acelerar()}Km/h...");
			Console.WriteLine($"Freando.. {uno.Frear()}Km/h...");
			Console.WriteLine($"Freando.. {uno.Frear()}Km/h...");

			Console.WriteLine();
			Ferrari ferrari1 = new Ferrari(); // Método Frear() herdado de Ferrari()... Acelera a 15Km/h e Frea a 15Km/h
			Console.WriteLine("Ferrari... herdando de Ferrari");
			Console.WriteLine($"Acelerando.. {ferrari1.Acelerar()}Km/h...");
			Console.WriteLine($"Acelerando.. {ferrari1.Acelerar()}Km/h...");
			Console.WriteLine($"Freando.. {ferrari1.Frear()}Km/h...");
			Console.WriteLine($"Freando.. {ferrari1.Frear()}Km/h...");
			Console.WriteLine($"Freando.. {ferrari1.Frear()}Km/h...");

			Console.WriteLine();
			Carro ferrari2 = new Ferrari(); // Método Frear() herdado de Carro()... Acelera a 15Km/h e Frea a 5Km/h
			Console.WriteLine("Ferrari... herdando de Carro");
			Console.WriteLine($"Acelerando.. {ferrari2.Acelerar()}Km/h...");
			Console.WriteLine($"Acelerando.. {ferrari2.Acelerar()}Km/h...");
			Console.WriteLine($"Freando.. {ferrari2.Frear()}Km/h...");
			Console.WriteLine($"Freando.. {ferrari2.Frear()}Km/h...");
			Console.WriteLine($"Freando.. {ferrari2.Frear()}Km/h...");
		}
	}
}
