﻿using System;

namespace CursoCSharp.OO {
	class Polimorfismo {
		public class Comida {
			public double Peso;

			public Comida(double peso) {
				Peso = peso;
			}

			public Comida() {
			}
		}
		public class Feijao : Comida {
			//public double Peso;
			public Feijao(double peso) : base(peso) { }
		}

		public class Arroz : Comida {
			//public double Peso;
		}

		public class Carne : Comida {
			//public double Peso;
		}

		public class Pessoa {
			public double Peso;

			//public void Comer(Feijao feijao)
			//{
			//	Peso += feijao.Peso;
			//}

			//public void Comer(Arroz arroz)
			//{
			//	Peso += arroz.Peso;
			//}

			//public void Comer(Carne carne)
			//{
			//	Peso += carne.Peso;
			//}

			public void Comer(Comida comida) {
				Peso += comida.Peso;
			}
		}
		public static void Executar() {
			Feijao ingrediente1 = new Feijao(.30);
			//ingrediente1.Peso = 0.3;

			Arroz ingrediente2 = new Arroz();
			ingrediente2.Peso = 0.25;

			Carne ingrediente3 = new Carne();
			ingrediente3.Peso = 0.3;

			Pessoa eu = new Pessoa();
			eu.Peso = 64.50;
			Console.WriteLine($"Eu pesava {eu.Peso}Kg");
			eu.Comer(ingrediente1);
			eu.Comer(ingrediente2);
			eu.Comer(ingrediente3);
			Console.WriteLine($"Depois de comer Arroz, Feijão e Carne eu agora peso {eu.Peso}Kg");

			Console.WriteLine();
		}
	}
}
