using System;

namespace CursoCSharp.OO {
	public class Animal {
		protected string Nome {
			get; set;
		}

		public Animal(string nome) {
			Nome = nome;
		}
	}

	public class Cachorro : Animal {
		double Altura {
			get; set;
		}

		public Cachorro(string nome) : base(nome) {
			Console.WriteLine($"Cachorro {nome} inicializado.");
		}

		public Cachorro(string nome, double altura) : this(nome) {
			Altura = altura;
		}

		public override string ToString() {
			return $"{Nome} tem {Altura}cm de altura.";
		}
	}

	class ConstrutorThis {
		public static void Executar() {
			var toto = new Cachorro("Totó");
			var valente = new Cachorro("Valente", 35.0);

			Console.WriteLine(toto);
			Console.WriteLine(valente.ToString());
		}
	}
}
