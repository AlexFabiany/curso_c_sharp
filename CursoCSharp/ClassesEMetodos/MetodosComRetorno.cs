﻿using System;

namespace CursoCSharp.ClassesEMetodos {

	class CalculadoraComum {
		public int Somar(int a, int b) {
			return a + b;
		}

		public int Subtrair(int a, int b) {
			return a - b;
		}

		public int Multiplicar(int a, int b) {
			return a * b;
		}

		public int Dividir(int a, int b) {
			return a / b;
		}
	}

	class CalculadoraCadeia {
		double memoria;

		public CalculadoraCadeia Somar(double a) {
			memoria += a;
			return this;
		}

		public CalculadoraCadeia Subtrair(double a) {
			memoria -= a;
			return this;
		}

		public CalculadoraCadeia Multiplicar(double a) {
			memoria *= a;
			return this;
		}

		public CalculadoraCadeia Dividir(double a) {
			memoria /= a;
			return this;
		}

		public CalculadoraCadeia Limpar() {
			memoria = 0;
			return this;
		}

		public CalculadoraCadeia Imprimir() {
			Console.WriteLine(memoria);
			return this;
		}

		public double Resultado() {
			return memoria;
		}
	}
	class MetodosComRetorno {
		public static void Executar() {
			var calc = new CalculadoraComum();
			var resultado = calc.Somar(5, 5);

			Console.WriteLine(resultado);
			Console.WriteLine(calc.Subtrair(2, 7));
			Console.WriteLine(calc.Multiplicar(4, 4));
			Console.WriteLine(calc.Dividir(25, 5));

			Console.WriteLine("---------------------");
			var calcF = new CalculadoraCadeia();
			calcF.Somar(3)
						.Imprimir()
							.Multiplicar(3)
								.Imprimir()
									.Subtrair(1)
										.Imprimir()
											.Dividir(2)
												.Imprimir()
													.Limpar()
														.Imprimir();
		}
	}
}
