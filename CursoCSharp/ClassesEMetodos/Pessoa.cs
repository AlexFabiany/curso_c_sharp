﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	class Pessoa {
		public string Nome;
		public int Idade;

		public string ApresentarSe() {
			return string.Format($"Olá! Me chamo {Nome} e tenho {Idade} anos.");
		}

		public void ApresentarSeNoConsole() {
			Console.WriteLine(ApresentarSe());
		}
	}
}
