﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	class DesafioAtributo {

		int a = 10;

		public static void Executar() {
			//Acessar a variável a dentro do método Executar!
			var acesso = new DesafioAtributo();
			Console.WriteLine(acesso.a);
		}
	}
}
