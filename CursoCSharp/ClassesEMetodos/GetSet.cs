﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	class GetSet {
		public class Moto {
			private string Marca;
			private string Modelo;
			private uint Cilindrada;
			public Moto() {

			}
			public Moto(string marca, string modelo, uint cilindrada) {
				//Marca = marca;
				//Modelo = modelo;
				//Cilindrada = cilindrada;
				SetMarca(marca);
				SetModelo(modelo);
				SetCilindrada(cilindrada);
			}

			public string GetMarca() {
				return Marca;
			}

			public void SetMarca(string marca) {
				Marca = marca;
			}

			public string GetModelo() {
				return Modelo;
			}

			public void SetModelo(string modelo) {
				Modelo = modelo;
			}

			public uint GetCilindrada() {
				return Cilindrada;
			}

			public void SetCilindrada(uint cilindrada) {
				//if(cilindrada > 0) {
				//  Cilindrada = cilindrada;
				//}

				//Cilindrada = Math.Abs(cilindrada);

				Cilindrada = cilindrada;
			}

			public override string ToString() {
				return string.Format("\nMarca: {0}\nModelo:{1}\nCilindrada:{2}\n", Marca, Modelo, Cilindrada);
			}

		}
		public static void Executar() {
			var moto1 = new Moto("Kawasaki", "Ninja ZX-6R", 636);
			Console.WriteLine("Moto 1 {0}", moto1.ToString());

			var moto2 = new Moto();
			moto2.SetMarca("Honda");
			moto2.SetModelo("CG Titan");
			moto2.SetCilindrada(150);
			Console.WriteLine("Moto 2 {0}", moto2.ToString());
		}
	}
}
