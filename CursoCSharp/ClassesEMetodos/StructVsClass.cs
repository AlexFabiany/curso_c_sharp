﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	public struct SPonto {
		public int X;
		public int Y;
	}

	public class CPonto {
		public int X;
		public int Y;
	}
	class StructVsClass {
		public static void Executar() {
			SPonto p1 = new SPonto { X = 1, Y = 3 };
			SPonto copiaP1 = p1; // Na struct a atribuição SEMPRE é por VALOR!
			p1.X = 3; // Não alterou o valor de copiaP1.X

			Console.WriteLine("Ponto 1 X: {0}", p1.X);
			Console.WriteLine("Cópia ponto 1 X: {0}", copiaP1.X);

			CPonto p2 = new CPonto { X = 2, Y = 4 };
			CPonto copiaP2 = p2; // Na class a atribuição SEMPRE é por REFERÊNCIA!
			p2.X = 4; // Alterou o valor de copiaP2.X

			Console.WriteLine("Ponto 2 X: {0}", p2.X);
			Console.WriteLine("Cópia ponto 2 X: {0}", copiaP2.X);
		}
	}
}
