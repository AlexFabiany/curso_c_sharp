﻿using System;

namespace CursoCSharp.ClassesEMetodos {

	public class Produto {
		public string Nome;
		public double Preco;
		public static double Desconto = 0.15;

		public Produto() {

		}
		public Produto(string nome, double preco, double desconto) {
			Nome = nome;
			Preco = preco;
			Desconto = desconto;
		}

		public double CalcularDesconto() {
			return Preco - Preco * Desconto;
		}
	}
	class AtributosEstaticos {
		public static void Executar() {
			var prod1 = new Produto("Caneta", 10, 0.15);
			var prod2 = new Produto() { Nome = "Borracha", Preco = 5 };

			Produto.Desconto = .10;
			Console.WriteLine("Produto 1: Valor com desconto {0}", prod1.CalcularDesconto());
			Console.WriteLine("Produto 2: Valor com desconto {0}", prod2.CalcularDesconto());
			Produto.Desconto = .50;
			Console.WriteLine("Produto 1: Valor com desconto {0}", prod1.CalcularDesconto());
			Console.WriteLine("Produto 2: Valor com desconto {0}", prod2.CalcularDesconto());
		}
	}
}
