﻿using System;

namespace CursoCSharp.ClassesEMetodos {

	interface Ponto {
		void MoverNaDagonal(int delta);
	}

	struct Coordenada : Ponto {
		public int X;
		public int Y;

		public Coordenada(int x, int y) {
			X = x;
			Y = y;
		}
		public void MoverNaDagonal(int delta) {
			X += delta;
			Y += delta;
		}
	}
	class Struct {
		public static void Executar() {
			Coordenada coordenadaInicial;
			coordenadaInicial.X = 2;
			coordenadaInicial.Y = 2;

			Console.WriteLine("Coordenada Inicial");
			Console.WriteLine("X = {0}", coordenadaInicial.X);
			Console.WriteLine("Y = {0}", coordenadaInicial.Y);

			Coordenada coordenadaFinal = new Coordenada(x: 9, y: 1);
			coordenadaFinal.MoverNaDagonal(10);
			Console.WriteLine("Coordenada Final");
			Console.WriteLine("X = {0}", coordenadaFinal.X);
			Console.WriteLine("Y = {0}", coordenadaFinal.Y);
		}
	}
}
