﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	public enum Genero { Ação, Animação, Aventura, Comédia, Terror };

	public class Filme {
		public string Titulo;
		public Genero Genero;

		public override string ToString() {
			return string.Format("\nTítulo: {0}\nGênero: {1}\n", Titulo, Genero);
		}
	}
	class Enumeracoes {
		public static void Executar() {
			//int id = (int) Genero.Animação;
			//Console.WriteLine(id);

			Filme novoFilme = new Filme();
			novoFilme.Titulo = "Sharknado 12";
			novoFilme.Genero = Genero.Comédia;
			Console.WriteLine(novoFilme.ToString());
		}
	}
}
