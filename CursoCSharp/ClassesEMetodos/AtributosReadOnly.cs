﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	public class Cliente {
		public string Nome;
		readonly DateTime Nascimento;

		public Cliente() {

		}

		public Cliente(string nome, DateTime nascimento) {
			Nome = nome;
			Nascimento = nascimento;
		}

		public override string ToString() {
			return string.Format("\nNome: {0}\nData de Nascimento: {1}\n", Nome, GetDataDeNascimento());
		}

		public string GetDataDeNascimento() {
			return string.Format("{0:D2}/{1:D2}/{2}", Nascimento.Day, Nascimento.Month, Nascimento.Year);
		}

	}
	class AtributosReadOnly {
		public static void Executar() {
			var cliente = new Cliente("Alex Fabiany", new DateTime(1972, 12, 13));
			Console.WriteLine(cliente.ToString());
		}
	}
}
