﻿using System;

namespace CursoCSharp.ClassesEMetodos {

	public class CalculadoraEstatica {
		public static double Somar(double value1, double value2) {
			return value1 + value2;
		}

		public static double Subtrair(double value1, double value2) {
			return value1 - value2;
		}

		public static double Multiplicar(double value1, double value2) {
			return value1 * value2;
		}

		public static double Dividir(double value1, double value2) {
			return value1 / value2;
		}
	}
	class MetodosEstaticos {
		public static void Executar() {
			var resultado = CalculadoraEstatica.Somar(2.3, 2.2);
			Console.WriteLine("Resultado: {0}", resultado);

			Console.WriteLine(CalculadoraEstatica.Multiplicar(2.3, 2.2));
		}
	}
}
