﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	class Membros {
		public static void Executar() {
			Pessoa p1 = new Pessoa();
			p1.Nome = "Alex";
			p1.Idade = 47;

			p1.ApresentarSeNoConsole();

			var p2 = new Pessoa();
			p2.Nome = "Marcíria Lima";
			p2.Idade = 47;

			string apresentacaoDeP2 = p2.ApresentarSe();
			Console.WriteLine(apresentacaoDeP2);
			Console.WriteLine(apresentacaoDeP2.Length);
		}
	}
}
