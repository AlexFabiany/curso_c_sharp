﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	class Construtores {

		class Carro {
			public string Modelo;
			public string Fabricante;
			public int Ano;

			public Carro() {

			}
			public Carro(string modelo, string fabricante, int Ano) {
				this.Modelo = modelo;
				this.Fabricante = fabricante;
				this.Ano = Ano;
			}
			public override string ToString() {
				return string.Format($"Fabricante: {this.Fabricante}\nModelo: {this.Modelo}\nAno: {this.Ano}\n");
			}
		}
		public static void Executar() {
			var carro1 = new Carro();
			carro1.Modelo = "325i";
			carro1.Fabricante = "BMW";
			carro1.Ano = 2018;
			Console.WriteLine(carro1.ToString());

			var carro2 = new Carro("Ka", "Ford", 2019);
			Console.WriteLine(carro2.ToString());

			var carro3 = new Carro() {
				Fabricante = "Fiat",
				Modelo = "Tempra",
				Ano = 2019
			};
			Console.WriteLine(carro3.ToString());
		}
	}
}
