﻿using System;

namespace CursoCSharp.ClassesEMetodos {
	class Props {
		class CarroOpcional {
			double desconto = .1;
			string nome;

			public string Nome {
				get {
					return string.Format("Opcional: {0}", nome);
				}
				set {
					nome = value;
				}
			}

			// Propriedade autoimplementada
			public double Preco {
				get; set;
			}

			// Somente leitura
			public double PrecoComDesconto {
				get => Preco - (desconto * Preco); // Lambda, lambda, lambda

				//get {
				//  return Preco - (desconto * Preco);
				//}
			}

			public CarroOpcional() {
			}

			public CarroOpcional(string nome, double preco) {
				Nome = nome;
				Preco = preco;
			}

			public override string ToString() {
				return string.Format("Nome: {0}\nPreço: {1}\nPreço com Desconto: {2}\n", Nome, Preco, PrecoComDesconto);
			}
		}
		public static void Executar() {
			var op1 = new CarroOpcional("Ar Condicionado", 3499.9);
			Console.WriteLine(op1.ToString());

			var op2 = new CarroOpcional();
			op2.Nome = "Direção Hidráulica";
			op2.Preco = 2349.9;
			Console.WriteLine(op2.ToString());
		}
	}
}
