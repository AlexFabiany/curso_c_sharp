﻿using System;

namespace CursoCSharp.MetodosEFuncoes {
	delegate double Operacao(double x, double y);
	class LambdasDelegate {
		public static void Executar() {
			Operacao soma = (x, y) => x + y;
			Operacao subt = (x, y) => x - y;
			Operacao mult = (x, y) => x * y;
			Operacao divi = (x, y) => x / y;

			Console.WriteLine($"Soma 3 + 3: {soma(3, 3)}");
			Console.WriteLine($"Subtração 5 - 3: {subt(5, 3)}");
			Console.WriteLine($"Miltiplicação 12 x 8: {mult(12, 8)}");
			Console.WriteLine($"Divisão 10 / 5: {divi(10, 5)}");
		}
	}
}
