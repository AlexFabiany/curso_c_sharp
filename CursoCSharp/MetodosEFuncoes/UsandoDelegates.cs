﻿using System;

namespace CursoCSharp.MetodosEFuncoes {
	class UsandoDelegates {
		delegate double Soma(double a, double b);
		delegate void ImprimirSoma(double a, double b);

		static double MinhaSoma(double x, double y) {
			return x + y;
		}

		static void MeuImprimirSoma(double x, double y) {
			Console.WriteLine(x + y);
		}
		public static void Executar() {
			var a = 3.5;
			var b = 8.3;
			var x = 13.89;
			var y = 34.32;

			Soma op1 = MinhaSoma;
			Console.WriteLine(op1(a, b));

			ImprimirSoma op2 = MeuImprimirSoma;
			op2(x, y);

			Func<double, double, double> op3 = MinhaSoma;
			Console.WriteLine(op3(y, x));

			Action<double, double> op4 = MeuImprimirSoma;
			op4(b, a);
		}
	}
}
