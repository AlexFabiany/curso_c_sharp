﻿using System;

namespace CursoCSharp.MetodosEFuncoes {
	class ExemploLambda {
		public static void Executar() {
			Action<string> algoNoConsole = (a) => {
				Console.WriteLine($"Lambda com C#!{a}");
			};

			algoNoConsole(" > parâmetro <");

			Func<int> jogarDado = () => {
				Random random = new Random();
				return random.Next(1, 7);
			};

			Console.WriteLine($"Jogando o dado: {jogarDado()}");

			Func<int, string> conversorHex = numero => numero.ToString("X");
			Console.WriteLine($"Convertendo 1971 em hexadecimal: {conversorHex(1971)}");

			Func<int, int, int, string> formatarData = (dia, mes, ano) =>
				string.Format("{0:D2}/{1:D2}/{2:D4}", dia, mes, ano);
			Console.WriteLine($"Formatando dia 13, mês 12 e ano 1971: {formatarData(13, 12, 1971)}");
		}
	}
}
