﻿using System;

namespace CursoCSharp.EstruturasDeControle {
	class UsandoContinue {
		public static void Executar() {
			int ultimoNumero = 50;

			Console.WriteLine("Números pares entre 1 e {0}", ultimoNumero);
			Console.WriteLine();
			// Forma burra
			Console.WriteLine("Forma burra");
			for (int i = 1; i <= ultimoNumero; i++) {
				if (i % 2 == 1) {
					continue;
				}

				Console.Write("{0} ", i);
			}
			Console.WriteLine();
			Console.WriteLine();
			//Forma inteligente
			Console.WriteLine("Forma inteligente");
			for (int i = 2; i <= ultimoNumero; i += 2) {
				Console.Write("{0} ", i);
			}
			Console.WriteLine();
		}
	}
}
