﻿using System;

namespace CursoCSharp {
	class EstruturaWhile {
		public static void Executar() {
			int palpite = 0;
			Random random = new Random();
			int numeroSecreto = random.Next(1, 16);
			bool numeroEncontrado = false;
			int tentativasRestantes = 5;
			int tentativas = 0;
			//var corAnterior = Console.BackgroundColor;

			while (tentativasRestantes > 0 && !numeroEncontrado) {
				Console.Write("Digite seu palpite entre 1-15 ({0} tentativas): ", tentativasRestantes);
				string entrada = Console.ReadLine();
				int.TryParse(entrada, out palpite);

				tentativas++;
				tentativasRestantes--;

				if (numeroSecreto == palpite) {
					numeroEncontrado = true;
					Console.BackgroundColor = ConsoleColor.Green;
					Console.ForegroundColor = ConsoleColor.Black;
					Console.WriteLine("Numero encontrado em {0} tentativas", tentativas);
					Console.ResetColor();
				} else if (palpite > numeroSecreto) {
					Console.BackgroundColor = ConsoleColor.Yellow;
					Console.ForegroundColor = ConsoleColor.Black;
					Console.WriteLine("Tente um número menor...");
					Console.ResetColor();
				} else {
					Console.BackgroundColor = ConsoleColor.Yellow;
					Console.ForegroundColor = ConsoleColor.Black;
					Console.WriteLine("Tente um número maior...");
					Console.ResetColor();
				}
			}
		}
	}
}
