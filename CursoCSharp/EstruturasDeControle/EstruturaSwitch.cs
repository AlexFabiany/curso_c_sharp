﻿using System;

namespace CursoCSharp.EstruturasDeControle {
	class EstruturaSwitch {
		public static void Executar() {
			Console.Write("Avalie o atendimento com nota de 1-5: ");
			int.TryParse(Console.ReadLine(), out int nota);

			switch (nota) {
				case 0: {
					Console.WriteLine($"Nota {nota} - Péssimo!");
					break;
				}
				case 1:
				case 2:
				Console.WriteLine($"Nota {nota} - Ruim!");
				break;

				case 3:
				Console.WriteLine($"Nota {nota} - Regular!");
				break;

				case 4:
				Console.WriteLine($"Nota {nota} - Bom!");
				break;

				case 5:
				Console.WriteLine($"Nota {nota} - Ótimo!");
				break;

				default:
				Console.WriteLine($"Nota {nota} - Nota inválida!");
				break;
			}

			Console.WriteLine("Obrigado por responder.");
		}
	}
}
