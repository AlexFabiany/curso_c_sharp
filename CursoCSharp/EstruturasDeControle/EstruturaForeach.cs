﻿using System;

namespace CursoCSharp.EstruturasDeControle {
	class EstruturaForeach {
		public static void Executar() {
			string palavra = "Olá Mundo!";

			foreach (char letra in palavra) {
				//Console.Write("{0} ", letra);
				Console.WriteLine(letra);
			}

			var alunos = new string[] { "Alex", "Andreas", "Marcíria" };

			foreach (string aluno in alunos) {
				Console.WriteLine(aluno);
			}
		}
	}
}
