﻿using System;

namespace CursoCSharp.EstruturasDeControle {
	class EstruturaIf {
		public static void Executar() {
			bool bomComportamento = false;
			string entrada;

			Console.Write("Digite a nota do aluno: ");
			entrada = Console.ReadLine();
			Double.TryParse(entrada, out double nota);

			Console.Write("O aluno tem bom comportamento (S/N)? ");
			entrada = Console.ReadLine();

			bomComportamento = entrada.ToLower() == "s";

			if (nota >= 9 && bomComportamento) {
				Console.WriteLine("Quadro de honra!");
			}
		}
	}
}
