﻿using System;

namespace CursoCSharp.TopicosAvancados {
	class Nullables {
		public static void Executar() {
			Nullable<int> num1 = null;
			int? num2 = null;

			if (num1.HasValue) {
				Console.WriteLine($"O valor de num1 é {num1}");
			} else {
				Console.WriteLine("A variável não possui valor!");
			}

			// Atribuindo valor padrão
			int num3 = num1 ?? 1000;
			Console.WriteLine(num3);

			bool? booleana = null;
			bool resultado = booleana.GetValueOrDefault();
			Console.WriteLine(resultado);

			try {
				int x = num1.Value; // num1.GetValueOrDefault();
				int y = num2.Value; // num2.GetValueOrDefault();
				Console.WriteLine(x + y);
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
		}
	}
}
