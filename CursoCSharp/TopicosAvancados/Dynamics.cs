﻿using System;

namespace CursoCSharp.TopicosAvancados {
	class Dynamics {
		public static void Executar() {
			dynamic meuObj = "teste";
			meuObj = 3;
			meuObj++;
			Console.WriteLine(meuObj);

			dynamic aluno = new System.Dynamic.ExpandoObject();
			aluno.Nome = "Alex";
			aluno.Idade = 47;
			aluno.Nota = 9.9;

			Console.WriteLine($"Nome: {aluno.Nome} Idade: {aluno.Idade} Nota: {aluno.Nota}");
		}
	}
}
