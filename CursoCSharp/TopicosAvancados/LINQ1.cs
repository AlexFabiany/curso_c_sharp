﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CursoCSharp.TopicosAvancados {
	public class Aluno {
		public string Nome;
		public int Idade;
		public double Nota;
	}
	class LINQ1 {
		public static void Executar() {
			var alunos = new List<Aluno> {
				new Aluno(){Nome = "Alex", Idade = 47, Nota = 10.0},
				new Aluno(){Nome = "Marcíria", Idade = 47, Nota = 9.7},
				new Aluno(){Nome = "Andreas", Idade = 24, Nota = 8.5},
				new Aluno(){Nome = "Ana", Idade = 25, Nota = 9.5},
				new Aluno(){Nome = "Pedro", Idade = 24, Nota = 8.0},
				new Aluno(){Nome = "André", Idade = 21, Nota = 4.3},
				new Aluno(){Nome = "Jorge", Idade = 18, Nota = 8.5},
				new Aluno(){Nome = "Júlia", Idade = 22, Nota = 7.5},
				new Aluno(){Nome = "Ana", Idade = 21, Nota = 7.7},
				new Aluno(){Nome = "Márcio", Idade = 18, Nota = 6.8}
			};
			Console.WriteLine("== Aprovados =======================");
			var aprovados = alunos.Where(a => a.Nota >= 7).OrderBy(a => -a.Nota);
			Console.WriteLine("{0,-10} {1,10} {2,10}", "Nome", "Idade", "Nota");
			Console.WriteLine("====================================");
			foreach (var aluno in aprovados) {
				Console.WriteLine("{0,-10} {1,10} {2,10:N2}", aluno.Nome, aluno.Idade, aluno.Nota);
			}

			Console.WriteLine("\n== Reprovados ======================");
			var reprovados = alunos.Where(a => a.Nota < 7).OrderBy(a => a.Nota); ;
			Console.WriteLine("{0,-10} {1,10} {2,10}", "Nome", "Idade", "Nota");
			Console.WriteLine("====================================");
			foreach (var aluno in reprovados) {
				Console.WriteLine("{0,-10} {1,10} {2,10:N2}", aluno.Nome, aluno.Idade, aluno.Nota);
			}

			Console.WriteLine("\n== Chamada =========================");
			var chamada = alunos.OrderBy(a => a.Nome).Select(a => a.Nome);
			Console.WriteLine("{0,-10}", "Nome");
			Console.WriteLine("====================================");
			foreach (var nome in chamada) {
				Console.WriteLine("{0,-10}", nome);
			}

			Console.WriteLine("\n== Aprovados (por Idade)===========");
			var alunosAprovados =
				from aluno in alunos
				where aluno.Nota >= 7
				orderby -aluno.Idade
				select aluno.Nome;

			Console.WriteLine("{0,-10}", "Nome");
			Console.WriteLine("====================================");
			foreach (var aluno in alunosAprovados) {
				Console.WriteLine("{0,-10}", aluno);
			}
		}
	}
}
