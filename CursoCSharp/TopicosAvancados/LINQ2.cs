﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CursoCSharp.TopicosAvancados {
	class LINQ2 {
		public static void Executar() {
			var alunos = new List<Aluno> {
				new Aluno(){Nome = "Alex", Idade = 47, Nota = 10.0},
				new Aluno(){Nome = "Marcíria", Idade = 47, Nota = 9.7},
				new Aluno(){Nome = "Andreas", Idade = 24, Nota = 8.5},
				new Aluno(){Nome = "Ana", Idade = 25, Nota = 9.5},
				new Aluno(){Nome = "Pedro", Idade = 24, Nota = 8.0},
				new Aluno(){Nome = "André", Idade = 21, Nota = 4.3},
				new Aluno(){Nome = "Jorge", Idade = 18, Nota = 8.5},
				new Aluno(){Nome = "Júlia", Idade = 22, Nota = 7.5},
				new Aluno(){Nome = "Ana", Idade = 21, Nota = 7.7},
				new Aluno(){Nome = "Márcio", Idade = 18, Nota = 6.8}
			};

			Console.WriteLine("== Alunos ================================");
			Console.WriteLine("{0,-10} {1,10} {2,10}", "Nome", "Idade", "Nota");
			foreach (var aluno in alunos) {
				Console.WriteLine("{0,-10} {1,10} {2,10:N2}", aluno.Nome, aluno.Idade, aluno.Nota);
			}
			Console.WriteLine("==========================================");

			var pedro = alunos.Single(aluno => aluno.Nome.Equals("Pedro"));
			Console.WriteLine($"Aluno: {pedro.Nome} Idade: {pedro.Idade} Nota: {pedro.Nota}");

			var fulano = alunos.SingleOrDefault(aluno => aluno.Nome.Equals("Julia"));
			if (fulano == null) {
				Console.WriteLine("Aluno inexistente!");
			} else {
				Console.WriteLine($"Aluno: {fulano.Nome} Idade: {fulano.Idade} Nota: {fulano.Nota}");
			}

			var ana = alunos.First(aluno => aluno.Nome.Equals("Ana"));
			Console.WriteLine($"Aluno: {ana.Nome} Idade: {ana.Idade} Nota: {ana.Nota}");

			var sicrano = alunos.FirstOrDefault(aluno => aluno.Nome.Equals("Julia"));
			if (sicrano == null) {
				Console.WriteLine("Aluno inexistente!");
			} else {
				Console.WriteLine($"Aluno: {sicrano.Nome} Idade: {sicrano.Idade} Nota: {sicrano.Nota}");
			}

			var outraAna = alunos.LastOrDefault(aluno => aluno.Nome.Equals("Ana"));
			Console.WriteLine($"Aluno: {outraAna.Nome} Idade: {outraAna.Idade} Nota: {outraAna.Nota}");

			Console.WriteLine("\n=== Pula 1 e pega 5 =============================");
			var exemploSkip = alunos.Skip(1).Take(5);
			foreach (var aluno in exemploSkip) {
				Console.WriteLine($"Aluno: {aluno.Nome} Idade: {aluno.Idade} Nota: {aluno.Nota}");
			};

			Console.WriteLine("\n=== Maior e menor nota ==========================");
			var maiorNota = alunos.Max(aluno => aluno.Nota);
			var menorNota = alunos.Min(aluno => aluno.Nota);
			Console.WriteLine($"Maior nota: {maiorNota}");
			Console.WriteLine($"Menor nota: {menorNota}");

			Console.WriteLine("\n=== Soma e Média das notas=======================");
			var soma = alunos.Sum(aluno => aluno.Nota);
			var media = alunos.Average(aluno => aluno.Nota);
			Console.WriteLine($"Soma das notas: {soma}");
			Console.WriteLine($"Média das notas: {media}");

			Console.WriteLine("\n=== Soma e Média das notas - aprovados ==========");
			var alunosAprovados =
				from aluno in alunos
				where aluno.Nota >= 7
				orderby aluno.Nome
				select (aluno.Nome, aluno.Idade, aluno.Nota);

			Console.WriteLine("{0,-10} {1,10} {2,10}", "Nome", "Idade", "Nota");
			foreach (var aluno in alunosAprovados) {
				Console.WriteLine("{0,-10} {1,10} {2,10:N2}", aluno.Nome, aluno.Idade, aluno.Nota);
			}

			Console.WriteLine("=================================");
			var somaAp = alunos.Where(a => a.Nota >= 7).Sum(aluno => aluno.Nota);
			var mediaAp = alunos.Where(a => a.Nota >= 7).Average(aluno => aluno.Nota);
			Console.WriteLine($"Soma das notas: {somaAp}");
			Console.WriteLine($"Média das notas: {mediaAp}");

			Console.WriteLine("\n=== Soma e Média das notas - reprovados =========");
			var alunosReprovados =
				from aluno in alunos
				where aluno.Nota < 7
				orderby aluno.Nome
				select (aluno.Nome, aluno.Idade, aluno.Nota);

			Console.WriteLine("{0,-10} {1,10} {2,10}", "Nome", "Idade", "Nota");
			foreach (var aluno in alunosReprovados) {
				Console.WriteLine("{0,-10} {1,10} {2,10:N2}", aluno.Nome, aluno.Idade, aluno.Nota);
			}

			Console.WriteLine("=================================");
			var somaRep = alunos.Where(a => a.Nota < 7).Sum(aluno => aluno.Nota);
			var mediaRep = alunos.Where(a => a.Nota < 7).Average(aluno => aluno.Nota);
			Console.WriteLine($"Soma das notas: {somaRep}");
			Console.WriteLine($"Média das notas: {mediaRep}");
		}
	}
}
