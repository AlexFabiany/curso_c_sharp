﻿using CursoCSharp.API;
using CursoCSharp.ClassesEMetodos;
using CursoCSharp.Colecoes;
using CursoCSharp.EstruturasDeControle;
using CursoCSharp.Excecoes;
using CursoCSharp.Fundamentos;
using CursoCSharp.MetodosEFuncoes;
using CursoCSharp.OO;
using CursoCSharp.TopicosAvancados;
using System;
using System.Collections.Generic;

namespace CursoCSharp {
	class Program {
		static void Main(string[] args) {
			var central = new CentralDeExercicios(new Dictionary<string, Action>() {
                // Fundamentos
                {"Primeiro Programa - Fundamentos", PrimeiroPrograma.Executar},
								{"Comentários - Fundamentos", Comentarios.Executar},
								{"Váriáveis e Constantes - Fundamentos", VariaveisEConstantes.Executar},
								{"Inferência de Tipos - Fundamentos", InferenciaDeTipos.Executar},
								{"Interpolação de Strings - Fundamentos", InterpolacaoDeStrings.Executar},
								{"Notação Ponto - Fundamentos", NotacaoPonto.Executar},
								{"Ler dados do console - Fundamentos", LerDadosDoConsole.Executar},
								{"Formatando números - Fundamentos", FormatandoNumeros.Executar},
								{"Conversões - Fundamentos", Conversoes.Executar},
								{"Operadores Aritméticos - Fundamentos", OperadoresAritmeticos.Executar},
								{"Operadores Relacionais - Fundamentos", OperadoresRelacionais.Executar},
								{"Operadores Lógico - Fundamentos", OperadoresLogicos.Executar},
								{"Operadores de Atribuição - Fundamentos", OperadoresDeAtribuicao.Executar},
								{"Operadores Unários - Fundamentos", OperadoresUnarios.Executar},
								{"Operador Ternário - Fundamentos", OperadorTernario.Executar},
                // Estruturas de Controle
                {"Estrutura If - Estruturas de Controle", EstruturaIf.Executar},
								{"Estrutura If/Else - Estruturas de Controle", EstruturaIfElse.Executar},
								{"Estrutura Switch - Estruturas de Controle", EstruturaSwitch.Executar},
								{"Estrutura While - Estruturas de Controle", EstruturaWhile.Executar},
								{"Estrutura Do While - Estruturas de Controle", EstruturaDoWhile.Executar},
								{"Estrutura For - Estruturas de Controle", EstruturaFor.Executar},
								{"Estrutura Foreach - Estruturas de Controle", EstruturaForeach.Executar},
								{"Usando o Break - Estruturas de Controle", UsandoBreak.Executar},
								{"Usando o Continue - Estruturas de Controle", UsandoContinue.Executar},
                // Classes e Métodos
                {"Membros - Classes E Métodos", Membros.Executar},
								{"Construtores - Classes E Métodos", Construtores.Executar},
								{"Métodos com Retorno - Classes E Métodos", MetodosComRetorno.Executar},
								{"Métodos Estáticos - Classes E Métodos", MetodosEstaticos.Executar},
								{"Atributos Estáticos - Classes E Métodos", AtributosEstaticos.Executar},
								{"Desafio Atributo - Classes E Métodos", DesafioAtributo.Executar},
								{"Parâmetros Variáveis - Classes E Métodos", Params.Executar},
								{"Parâmetros Nomeados - Classes E Métodos", ParametrosNomeados.Executar},
								{"Getters & Setters - Classes E Métodos", GetSet.Executar},
								{"Atributos Readonly - Classes E Métodos", AtributosReadOnly.Executar},
								{"Enumerações - Classes E Métodos", Enumeracoes.Executar},
								{"Struct - Classes E Métodos", Struct.Executar},
								{"Struct vs Class - Classes E Métodos", StructVsClass.Executar},
								{"Atribuição por Valor vs Referência - Classes E Métodos", ValorVsReferencia.Executar},
								{"Parâmetros por Referência - Classes E Métodos", ParametrosPorReferencia.Executar},
								{"Parâmetros com Valor Padrão - Classes E Métodos", ParametroPadrao.Executar},
                // Coleções
                {"Arrays - Coleções", Colecoes.Array.Executar},
								{"List - Coleções", Listas.Executar},
								{"ArrayList - Coleções", ColecoesArrayList.Executar},
								{"Set - Coleções", ColecoesSet.Executar},
								{"Queue - Coleções", ColecoesQueue.Executar},
								{"Equal e GetHashCode - Coleções", EqualsEGetHashCode.Executar},
								{"Stack - Coleções", ColecoesStack.Executar},
								{"Dictionary - Coleções", ColecoesDictionary.Executar},
								// OO
								{"Herança - OO", Heranca.Executar},
								{"Construtor This - OO", ConstrutorThis.Executar},
								{"Encapsulamento - OO", OO.Encapsulamento.Executar},
								{"Polimorfismo - OO", Polimorfismo.Executar},
								{"Classe Abstrata - OO", Abstract.Executar},
								{"Interface - OO", Interface.Executar},
								{"Classes Sealed - OO", Sealed.Executar},
								// Métodos e Funções
								{"Lambda - Métodos e Funções", ExemploLambda.Executar},
								{"Delegate com Lambda - Métodos e Funções", LambdasDelegate.Executar},
								{"Usando Delegates - Métodos e Funções", UsandoDelegates.Executar},
								{"Delegate com Funções Anônimas - Métodos e Funções", DelegateFuncAnonima.Executar},
								{"Delegates como Parâmetros - Métodos e Funções", DelegatesComoParametros.Executar},
								{"Métodos de Extensão - Métodos e Funções", MetodosDeExtensao.Executar},
								// Exceções
								{"Primeira Exceção - Exceções", PrimeiraExcecao.Executar},
								{"Exceções Personalizadas - Exceções", ExcecoesPersonalizadas.Executar},
								// API
								{"Escrevendo Arquivo - Usando a API", EscrevendoArquivo.Executar},
								{"Lendo Arquivo - Usando a API", LendoArquivo.Executar},
								{"FileInfo - Usando a API", ExemploFileInfo.Executar},
								{"Trabalhando com Diretórios - Usando a API", Diretorios.Executar},
								{"DirectoryInfo - Usando a API", ExemploDirectoryInfo.Executar},
								{"Trabalhando com Path - Usando a API", ExemploPath.Executar},
								{"Trabalhando com Datas [DateTime] - Usando a API", ExemploDateTime.Executar},
								{"Trabalhando com Intervalos de Tempo [TimeSpan] - Usando a API", ExemploTimeSpan.Executar},
								// Tópicos Avançados
								{"LINQ #01 - Tópicos Avançados", LINQ1.Executar},
								{"LINQ #02 - Tópicos Avançados", LINQ2.Executar},
								{"Nullables - Tópicos Avançados", Nullables.Executar},
								{"Dynamics - Tópicos Avançados", Dynamics.Executar},
								{"Generics - Tópicos Avançados", Genericos.Executar},
			});

			central.SelecionarEExecutar();
		}
	}
}