﻿using System;
using System.IO;

namespace CursoCSharp.API {
	class LendoArquivo {
		public static void Executar() {
			var path = @"~/lendo_arquivo.txt".ParseHome();

			if (!File.Exists(path)) {
				using (StreamWriter sw = File.AppendText(path)) {
					sw.WriteLine("Produto;Preço;Quantidade");
					sw.WriteLine("Caneta BIC preta;3.59;89");
					sw.WriteLine("Borracha branca;2.89;27");
				}
			}

			try {
				using (StreamReader sr = new StreamReader(path)) {
					var texto = sr.ReadToEnd();
					//foreach (var linha in texto) {
					//	Console.WriteLine($"Linha -> {linha}");
					//}
					Console.WriteLine(texto);
				}
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
		}
	}
}
