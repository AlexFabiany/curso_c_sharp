﻿using System;

namespace CursoCSharp.API {
	class ExemploTimeSpan {
		public static void Executar() {
			var intervalo = new TimeSpan(days: 10, hours: 20, minutes: 30, seconds: 40);

			Console.WriteLine(intervalo);
			Console.WriteLine("Intervalo: {0} Dias, {1} Horas, {2} Minutos, {3} Segundos", intervalo.Days, intervalo.Hours, intervalo.Minutes, intervalo.Seconds);
			Console.WriteLine("=========================================================");
			Console.WriteLine("Só minutos:" + intervalo.Minutes);
			Console.WriteLine("Intervalo em horas: " + intervalo.TotalHours);
			Console.WriteLine("Intervalo em minutos: " + intervalo.TotalMinutes);

			Console.WriteLine("\n=========================================================");
			var largada = DateTime.Now;
			Console.WriteLine("Largada: " + largada);

			var chegada = DateTime.Now.AddMinutes(15);
			Console.WriteLine("Chegada: " + chegada);

			var intervaloDeTempo = chegada - largada;
			Console.WriteLine("Duração: " + intervaloDeTempo);

			Console.WriteLine("\n=========================================================");
			Console.WriteLine("Adicionanndo 16min a intervalo: " + intervalo.Add(TimeSpan.FromMinutes(16))); // Não altera intervalo original
			Console.WriteLine("Subtraindo 16min a intervalo: " + intervalo.Subtract(TimeSpan.FromMinutes(16))); // Não altera intervalo original
			Console.WriteLine("Intervalo: " + intervalo);
			Console.WriteLine("\nFormatações =============================================");
			Console.WriteLine("ToString('g'): " + intervalo.ToString("g"));
			Console.WriteLine("ToString('G'): " + intervalo.ToString("G"));
			Console.WriteLine("ToString('c'): " + intervalo.ToString("c"));
			Console.WriteLine("\nParse ===================================================");
			Console.WriteLine("Parse 01:02:03 em horas: " + TimeSpan.Parse("01:02:03").TotalHours);
			Console.WriteLine("Parse 01:02:03 em minutos: " + TimeSpan.Parse("01:02:03").TotalMinutes);
			Console.WriteLine("Parse 01:02:03 em segundos: " + TimeSpan.Parse("01:02:03").TotalSeconds);
		}
	}
}
