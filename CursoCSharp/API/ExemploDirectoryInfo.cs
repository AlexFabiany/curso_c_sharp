﻿using System;
using System.IO;

namespace CursoCSharp.API {
	class ExemploDirectoryInfo {
		public static void Executar() {
			var dirProjeto = @"D:/Projetos CSharp/CursoCSharp/CursoCSharp";
			var dirInfo = new DirectoryInfo(dirProjeto);

			if (!dirInfo.Exists) {
				dirInfo.Create();
			}

			Console.WriteLine("== Informações do Projetos =============");
			Console.WriteLine($"Raiz: {dirInfo.Root}");
			Console.WriteLine($"Nome completo: {dirInfo.FullName}");
			Console.WriteLine($"Diretório pai: {dirInfo.Parent}");
			Console.WriteLine($"Criação: {dirInfo.CreationTime}");
			Console.WriteLine($"Hash: {dirInfo.GetHashCode()}");
			Console.WriteLine($"Último acesso: {dirInfo.LastAccessTime}");

			Console.WriteLine("\n\n== Diretórios =============");
			var pastas = dirInfo.GetDirectories();
			foreach (var pasta in pastas) {
				Console.WriteLine(pasta);
			}

			Console.WriteLine("\n\n== Arquivos =============");
			var arquivos = dirInfo.GetFiles();
			foreach(var arquivo in arquivos) {
				Console.WriteLine(arquivo);
			}			
		}
	}
}
