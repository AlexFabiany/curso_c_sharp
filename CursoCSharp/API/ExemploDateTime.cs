﻿using System;

namespace CursoCSharp.API {
	class ExemploDateTime {
		public static void Executar() {
			var dateTime = new DateTime(day: 13, month: 12, year: 1971);

			Console.WriteLine($"Dia: {dateTime.Day}");
			Console.WriteLine($"Mês: {dateTime.Month}");
			Console.WriteLine($"Ano: {dateTime.Year}");

			// Sem horas
			var hoje = DateTime.Today;
			Console.WriteLine($"Hoje: {hoje}");

			// Com hora
			var diaAtual = DateTime.Now;
			Console.WriteLine("\n== Dia atual ====================");
			Console.WriteLine($"Dia atual: {diaAtual.Date}");
			Console.WriteLine($"Hora: {diaAtual.Hour}");
			Console.WriteLine($"Minutos: {diaAtual.Minute}");
			Console.WriteLine($"Segundos: {diaAtual.Second}");

			Console.WriteLine("\n== Ontem ====================");
			var ontem = diaAtual.AddDays(-1);
			Console.WriteLine($"Ontem: {ontem}");

			Console.WriteLine("\n== Amanhã ====================");
			var amanha = diaAtual.AddDays(1);
			Console.WriteLine($"Amanhã: {amanha}");

			Console.WriteLine("\n== Mês que vem ====================");
			var mesQueVem = diaAtual.AddDays(DateTime.DaysInMonth(diaAtual.Year, diaAtual.Month));
			Console.WriteLine($"Mês que vem: {mesQueVem}");

			Console.WriteLine("\n== Datas formatadas ====================");
			Console.WriteLine("dd: " + diaAtual.ToString("dd"));
			Console.WriteLine("d: " + diaAtual.ToString("d"));
			Console.WriteLine("D: " + diaAtual.ToString("D"));
			Console.WriteLine("g: " + diaAtual.ToString("g"));
			Console.WriteLine("G: " + diaAtual.ToString("G"));
			Console.WriteLine("dd-MM-yyyy HH:mm: " + diaAtual.ToString("dd-MM-yyyy HH:mm"));
		}
	}
}
