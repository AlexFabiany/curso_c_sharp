﻿using System;
using System.IO;

namespace CursoCSharp.API {
	class ExemploPath {
		public static void Executar() {
			var arquivo = @"~/exemplo_path_arquivo.txt".ParseHome();
			var pasta = @"~/exemplo_path_pasta".ParseHome();

			if (!File.Exists(arquivo)) {
				using (StreamWriter sw = File.CreateText(arquivo)) {
					sw.WriteLine("Um novo arquivo criado!");
				}
			}

			if (!Directory.Exists(pasta)) {
				Directory.CreateDirectory(pasta);
			}

			Console.WriteLine("== Informações de arquivo =============");
			Console.WriteLine($"Raiz: {Path.GetPathRoot(arquivo)}");
			Console.WriteLine($"Pasta: {Path.GetDirectoryName(arquivo)}");
			Console.WriteLine($"Nome: {Path.GetFileNameWithoutExtension(arquivo)}");
			Console.WriteLine($"Extensão: {Path.GetExtension(arquivo)}");

			Console.WriteLine("\n\n== Informações de pasta =============");
			Console.WriteLine($"Raiz: {Path.GetPathRoot(pasta)}");
			Console.WriteLine($"Nome completo: {Path.GetFullPath(pasta)}");
		}
	}
}
