﻿using System;
using System.Collections;

namespace CursoCSharp.Colecoes {
	class ColecoesStack {
		public static void Executar() {
			Stack pilha = new Stack();

			pilha.Push(3);
			pilha.Push("Alex");
			pilha.Push(true);
			pilha.Push(3.14f);

			Console.WriteLine($"Qtde de itens: {pilha.Count}\n");
			ImprimirPilha(pilha);
			Console.WriteLine($"Pop: {pilha.Pop()}\n");
			Console.WriteLine($"Qtde de itens: {pilha.Count}\n");
			Console.WriteLine($"Peek: {pilha.Peek()}\n");
			Console.WriteLine($"Qtde de itens: {pilha.Count}\n");
		}

		private static void ImprimirPilha(Stack pilha) {
			foreach (var item in pilha) {
				Console.WriteLine($"{item.GetType()}=>{item} ");
			}
			Console.WriteLine("\n");
		}
	}
}
