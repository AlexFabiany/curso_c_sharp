﻿using System;
using System.Collections.Generic;

namespace CursoCSharp.Colecoes {
	class ColecoesSet {
		public static void Executar() {
			Produto livro = new Produto("Game of Thrones", 49.99);
			HashSet<Produto> carrinho = new HashSet<Produto> {
				livro
			};

			HashSet<Produto> combo = new HashSet<Produto> { // Poderia ser List<Produto>
				new Produto("Camiseta GoT", 59.9),
				new Produto("DVD 1ª Temporada GoT", 29.09),
				new Produto("Poster 1ª Temporada GoT", 25.9),
				livro // Não funciona... HashSet não aceita duplicações
			};
			//carrinho.AddRange(combo);
			carrinho.UnionWith(combo);
			carrinho.Add(livro); // Não funciona... HashSet não aceita duplicações
			ImprimirCarrinho(carrinho);
		}

		private static void ImprimirCarrinho(HashSet<Produto> carrinho) {
			double precoTotal = 0;
			Console.WriteLine("Produtos\n---------------");
			foreach (var produto in carrinho) {
				//Console.WriteLine($"{carrinho.IndexOf(produto) + 1} - {produto.ToString()}");
				Console.WriteLine($"{produto.ToString()}");
				precoTotal += produto.Preco;
			}
			Console.WriteLine("---------------");
			Console.WriteLine($"Qtde de Produtos: {carrinho.Count}\nTotal: {precoTotal.ToString("R$ ###,###.#0")}");
		}
	}
}
