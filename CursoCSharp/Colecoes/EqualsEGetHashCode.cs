﻿using System;

namespace CursoCSharp.Colecoes {
	class EqualsEGetHashCode {
		public static void Executar() {
			Produto p1 = new Produto("Caneta", 1.89);
			Produto p2 = new Produto("Caneta", 1.89);

			Console.WriteLine(p1 == p2);
			Console.WriteLine(p1.Equals(p2));


		}
	}
}
