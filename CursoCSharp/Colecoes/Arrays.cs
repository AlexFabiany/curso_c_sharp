﻿using System;

namespace CursoCSharp.Colecoes {
	class Array {
		public static void Executar() {
			string[] alunos = new string[5];
			alunos[0] = "Alex";
			alunos[1] = "Marcíria";
			alunos[2] = "Andreas";
			alunos[3] = "Valente";
			alunos[4] = "Mariazinha";

			foreach (var aluno in alunos) {
				Console.WriteLine($"Aluno: {aluno}");
			}

			double somatorio = 0;
			double[] notas = { 7.8, 9.0, 5.7, 6.9, 8.5 };
			Console.WriteLine("------");
			Console.WriteLine("Notas");
			Console.WriteLine("------");

			foreach (var nota in notas) {
				somatorio += nota;
				Console.WriteLine($"{nota}");
			}

			Console.WriteLine("------");
			double media = somatorio / notas.Length;
			Console.WriteLine($"Média: {media}");

			char[] letras = { 'A', 'r', 'r', 'a', 'y' };
			string palavra = new string(letras);
			Console.WriteLine("------");
			Console.Write("Letras: ");
			foreach (var letra in letras) {
				Console.Write("{0} ", letra);
			}
			Console.WriteLine();
			Console.WriteLine($"Palavra: {palavra}");
		}
	}
}
