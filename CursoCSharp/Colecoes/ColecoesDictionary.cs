﻿using System;
using System.Collections.Generic;

namespace CursoCSharp.Colecoes {
	class ColecoesDictionary {
		public static void Executar() {
			Dictionary<int, string> filmes = new Dictionary<int, string>();
			filmes.Add(2000, "Gadiador");
			filmes.Add(2002, "Homem-Aranha");
			filmes.Add(2004, "Os Incríveis");
			filmes.Add(2006, "O Grande Truque");

			if (filmes.ContainsKey(2004)) {
				Console.WriteLine($"2004: {filmes[2004]}");
				Console.WriteLine($"2004: {filmes.GetValueOrDefault(2004)}");
			}

			Console.WriteLine();
			Console.WriteLine(filmes.ContainsValue("Amnésia"));

			//Console.WriteLine();
			//Console.WriteLine($"Removeu? {filmes.Remove(2004)}");

			Console.WriteLine();
			Console.WriteLine(filmes.TryGetValue(2006, out string filme2006));
			Console.WriteLine($"Filme {filme2006}");

			Console.WriteLine();
			foreach (var chave in filmes.Keys) {
				Console.WriteLine(chave);
			}

			Console.WriteLine();
			foreach (var valor in filmes.Values) {
				Console.WriteLine(valor);
			}

			Console.WriteLine();
			foreach (KeyValuePair<int, string> filme in filmes) {
				Console.WriteLine($"O filme {filme.Value} é de {filme.Key}");
			}

			Console.WriteLine();
			foreach (var filme in filmes) {
				Console.WriteLine($"{filme.Key}: {filme.Value}");
			}
		}
	}
}
