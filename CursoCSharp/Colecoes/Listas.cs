﻿using System;
using System.Collections.Generic;

namespace CursoCSharp.Colecoes {
	public class Produto {
		public string Nome;
		public double Preco;

		public Produto(string nome, double preco) {
			Nome = nome;
			Preco = preco;
		}

		public override bool Equals(object obj) {
			return obj is Produto produto &&
						 Nome == produto.Nome &&
						 Preco == produto.Preco;
		}

		public override int GetHashCode() {
			return HashCode.Combine(Nome, Preco);
		}

		public override string ToString() {
			return string.Format($"{Nome.PadRight(30)}\t{Preco.ToString("R$ ###,###.00")}");
		}
	}
	class Listas {
		public static void Executar() {
			Produto livro = new Produto("Game of Thrones", 49.99);
			List<Produto> carrinho = new List<Produto> {
				livro
			};

			List<Produto> combo = new List<Produto> {
				new Produto("Camiseta GoT", 59.9),
				new Produto("DVD 1ª Temporada GoT", 29.09),
				new Produto("Poster 1ª Temporada GoT", 25.9)
			};
			carrinho.AddRange(combo);
			ImprimirCarrinho(carrinho);
		}

		private static void ImprimirCarrinho(List<Produto> carrinho) {
			double precoTotal = 0;
			Console.WriteLine("Produtos\n---------------");
			foreach (var produto in carrinho) {
				Console.WriteLine($"{carrinho.IndexOf(produto) + 1} - {produto.ToString()}");
				precoTotal += produto.Preco;
			}
			Console.WriteLine("---------------");
			Console.WriteLine($"Qtde de Produtos: {carrinho.Count}\nTotal: {precoTotal.ToString("R$ ###,###.#0")}");
		}
	}
}
