﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CursoCSharp.Colecoes {
	class ColecoesQueue {
		public static void Executar() {
			var fila = new Queue<string>();

			fila.Enqueue("Alex Fabiany");
			fila.Enqueue("Marcíria Lima");
			fila.Enqueue("Valente Leão");
			fila.Enqueue("Alex Fabiany"); // Aceita duplicação

			Console.WriteLine(fila.Peek()); // Pega o primeiro da fila, sem desenfileirar
			Console.WriteLine(fila.Count);

			ImprimirLista(fila);

			//Console.WriteLine(fila.Dequeue()); // Desenfileira
			//Console.WriteLine(fila.Count);

			var salada = new Queue();
			salada.Enqueue(3);
			salada.Enqueue("Alex Fabiany");
			salada.Enqueue(true);
			salada.Enqueue(3.14);

			Console.WriteLine("------------------");
			ImprimirLista(salada);
			Console.WriteLine(salada.Contains("Alex Fabiany"));
		}

		private static void ImprimirLista(Queue<string> lista) {
			foreach (var item in lista) {
				Console.WriteLine(item);
			}
		}

		private static void ImprimirLista(Queue lista) {
			foreach (var item in lista) {
				Console.WriteLine(item);
			}
		}
	}
}
