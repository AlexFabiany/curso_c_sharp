﻿using System;

namespace CursoCSharp.Fundamentos {
	class VariaveisEConstantes {
		public static void Executar() {
			// Área de circunferência
			double raio = 4.5;
			const double PI = 3.14;

			raio = 5.5;
			//PI = 3.1415;

			double area = PI * raio * raio;
			Console.WriteLine("A área da circunferência é: " + area);
			Console.WriteLine("");

			// Tipos internos
			bool estaChovendo = true;
			Console.WriteLine("está chovendo " + estaChovendo);
			Console.WriteLine("");

			byte varByteMin = byte.MinValue;
			byte varByteMax = byte.MaxValue;
			Console.WriteLine(value: "O menor valor de byte é: " + varByteMin);
			Console.WriteLine(value: "O maior valor de byte é: " + varByteMax);
			Console.WriteLine("");

			sbyte varSByteMin = sbyte.MinValue;
			sbyte varSByteMax = sbyte.MaxValue;
			Console.WriteLine("O menor valor de sbyte é: " + varSByteMin);
			Console.WriteLine("O maior valor de sbyte é: " + varSByteMax);
			Console.WriteLine("");

			short varShortMin = short.MinValue;
			short varShortMax = short.MaxValue;
			Console.WriteLine("O menor valor de short é: " + varShortMin);
			Console.WriteLine("O maior valor de short é: " + varShortMax);
			Console.WriteLine("");

			ushort varUShortMin = ushort.MinValue;
			ushort varUShortMax = ushort.MaxValue;
			Console.WriteLine("O menor valor de ushort é: " + varUShortMin);
			Console.WriteLine("O maior valor de ushort é: " + varUShortMax);
			Console.WriteLine("");

			int varIntMin = int.MinValue;
			int varIntMax = int.MaxValue;
			Console.WriteLine("O menor valor de int é: " + varIntMin);
			Console.WriteLine("O maior valor de int é: " + varIntMax);
			Console.WriteLine("");

			uint varUIntMin = uint.MinValue;
			uint varUIntMax = uint.MaxValue;
			uint populacaoBrasileira = 207_600_000;
			Console.WriteLine("O menor valor de uint é: " + varUIntMin);
			Console.WriteLine("O maior valor de uint é: " + varUIntMax);
			Console.WriteLine("População brasileira: " + populacaoBrasileira);
			Console.WriteLine("");

			long varLongMin = long.MinValue;
			long varLongMax = long.MaxValue;
			Console.WriteLine("O menor valor de long é: " + varLongMin);
			Console.WriteLine("O maior valor de long é: " + varLongMax);
			Console.WriteLine("");

			ulong varULongMin = ulong.MinValue;
			ulong varULongMax = ulong.MaxValue;
			ulong populacaoMundial = 7_600_000_000;
			Console.WriteLine("O menor valor de ulong é: " + varULongMin);
			Console.WriteLine("O maior valor de ulong é: " + varULongMax);
			Console.WriteLine("População mundial: " + populacaoMundial);
			Console.WriteLine("");

			float varFloatMin = float.MinValue;
			float varFloatMax = float.MaxValue;
			float precoUnitario = 1299.99f;
			Console.WriteLine("O menor valor de float é: " + varFloatMin);
			Console.WriteLine("O maior valor de float é: " + varFloatMax);
			Console.WriteLine("Preço unitário: " + precoUnitario);
			Console.WriteLine("");

			double varDoubleMin = double.MinValue;
			double varDoubleMax = double.MaxValue;
			double valorDeMercadoDaApple = 1_000_000_000_000.00;
			Console.WriteLine("O menor valor de double é: " + varDoubleMin);
			Console.WriteLine("O maior valor de double é: " + varDoubleMax);
			Console.WriteLine("Valor de mercado da Apple: " + valorDeMercadoDaApple);
			Console.WriteLine("");

			decimal varDecimalMin = decimal.MinValue;
			decimal varDecimalMax = decimal.MaxValue;
			Console.WriteLine("O menor valor de decimal é: " + varDecimalMin);
			Console.WriteLine("O maior valor de decimal é: " + varDecimalMax);
			Console.WriteLine("");

			char varCharMin = char.MinValue;
			char varCharMax = char.MaxValue;
			Console.WriteLine("O menor valor de char é: " + varCharMin);
			Console.WriteLine("O maior valor de char é: " + varCharMax);
			Console.WriteLine("char: apenas uma letra (character) entre aspas simples");
			Console.WriteLine("");

			string frase = "Bem vindo ao curso de C#";
			Console.WriteLine(frase);
			Console.WriteLine("string: sequência de char entre aspas duplas");
			Console.WriteLine("");
		}
	}

}
