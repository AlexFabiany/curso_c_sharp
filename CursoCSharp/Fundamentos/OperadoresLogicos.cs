﻿using System;

namespace CursoCSharp.Fundamentos {
	class OperadoresLogicos {
		public static void Executar() {
			var executouTrabalho1 = true;
			var executouTrabalho2 = false;

			Console.WriteLine($"Executou o trabalho da terça? {((executouTrabalho1) ? "Sim" : "Não")}");
			Console.WriteLine($"Executou o trabalho da quinta? {((executouTrabalho2) ? "Sim" : "Não")}");
			Console.WriteLine();

			var comprouTv32 = executouTrabalho1 ^ executouTrabalho2;
			bool comprouTv50 = executouTrabalho1 && executouTrabalho2;
			var comprouSorvete = executouTrabalho1 || executouTrabalho2;

			Console.WriteLine($"Comprou a Tv de 32pol? {((comprouTv32) ? "Sim" : "Não")}");
			Console.WriteLine($"Comprou a Tv de 50pol? {((comprouTv50) ? "Sim" : "Não")}");
			Console.WriteLine($"Comprou sorvete? {((comprouSorvete) ? "Sim" : "Não")}");
			Console.WriteLine($"Mais saudável? {((!comprouSorvete) ? "Sim" : "Não")}");
		}
	}
}
