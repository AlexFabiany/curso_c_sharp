﻿using System;

namespace CursoCSharp.Fundamentos {
	class OperadorTernario {
		public static void Executar() {
			bool bomComportamento = true;

			Console.Write("Digite uma nota: ");
			Double.TryParse(Console.ReadLine(), out double nota);

			string resultado = nota >= 7.0 && bomComportamento ? "Aprovado" : "Reprovado";

			Console.WriteLine($"Nota {nota}, {resultado}");
		}
	}
}
