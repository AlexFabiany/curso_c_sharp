﻿using System;

namespace CursoCSharp.Fundamentos {
	class LerDadosDoConsole {
		public static void Executar() {
			Console.Write("Qual é o seu nome? ");
			string nome = Console.ReadLine();

			Console.Write("Qua é a sua idade? ");
			int idade = int.Parse(Console.ReadLine());

			Console.Write("Qual é o seu salário? ");
			double salario = double.Parse(Console.ReadLine());

			//using System.Globalization
			//double salario = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
			// Ignora formatação do sistema operacional e aplica padrão para tipos double, float, etc

			Console.WriteLine($"{nome}, {idade} anos, salário: R${salario}");
		}
	}
}
