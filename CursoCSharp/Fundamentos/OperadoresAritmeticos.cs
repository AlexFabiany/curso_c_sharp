﻿using System;

namespace CursoCSharp.Fundamentos {
	class OperadoresAritmeticos {
		public static void Executar() {
			//Valor com desconto
			var preco = 1000.0;
			var imposto = 355;
			var desconto = 0.1;

			double total = preco + imposto;
			var totalComDesconto = total - (total * desconto);
			Console.WriteLine("Valor com desconto");
			Console.WriteLine($"Preço: {preco}");
			Console.WriteLine($"Imposto: {imposto}");
			Console.WriteLine($"Valor total: {preco + imposto}");
			Console.WriteLine($"Desconto: {desconto * 100}%");
			Console.WriteLine($"O valor final é: {totalComDesconto}");
			Console.WriteLine("======================================");
			// IMC
			Console.WriteLine("IMC");
			double peso = 64.5;
			double altura = 1.76;
			double imc = peso / Math.Pow(altura, 2);
			Console.WriteLine($"Peso: {peso}");
			Console.WriteLine($"Altura: {altura}");
			Console.WriteLine($"IMC: {imc}");
			Console.WriteLine("======================================");

			// Par / Impar
			Console.WriteLine("Par ou ímpar?");
			int par = 24;
			int impar = 55;
			Console.WriteLine($"O valor {par}/2 tem resto {par % 2}");
			Console.WriteLine($"O valor {impar}/2 tem resto {impar % 2}");
			Console.WriteLine("======================================");
		}
	}
}
