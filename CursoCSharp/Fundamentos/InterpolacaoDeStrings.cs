﻿using System;

namespace CursoCSharp.Fundamentos {
	class InterpolacaoDeStrings {
		public static void Executar() {
			string nome = "Notebook Gamer";
			string marca = "Dell";
			double preco = 5800.00;
			double numero1 = 5.25;
			double numero2 = 5.25;

			Console.WriteLine("O " + nome + " da marca " + marca + " custa " + preco + ".");
			Console.WriteLine("O {0} da marca {1} custa {2}.", nome, marca, preco);
			Console.WriteLine($"O {nome} da marca {marca} custa {preco}.");
			Console.WriteLine($"O resultado de {numero1} + {numero2} é {numero1 + numero2}.");
		}
	}
}
