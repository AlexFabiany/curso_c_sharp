﻿using System;

namespace CursoCSharp.Fundamentos {
	class NotacaoPonto {
		public static void Executar() {
			string saudacao = "Olá".Insert(3, ", world!").Replace("world", "mundo").ToUpper();
			Console.WriteLine(saudacao);

			string valorImportante = null;
			valorImportante = "Definida!";
			Console.WriteLine($"O tamanho da expressão '{((valorImportante != null) ? valorImportante : "nulo")}' é: {valorImportante?.Length}");
			// ? = navegação segura... só acessa o Length se valorImportante tiver sido definida
		}
	}
}
