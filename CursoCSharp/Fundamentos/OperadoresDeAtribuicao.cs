﻿using System;

namespace CursoCSharp.Fundamentos {
	class OperadoresDeAtribuicao {
		public static void Executar() {
			var num1 = 3;
			num1 = 7;
			Console.WriteLine($"Valor inicial de num1 é: {num1}");
			Console.WriteLine($"num1 += 10 é: {num1 += 10}"); // num1 = num1 + 10;
			Console.WriteLine($"num1 -= 3 é: {num1 -= 3}"); // num1 = num1 - 3;
			Console.WriteLine($"num1 *= 5 é: {num1 *= 5}"); // num1 = num1 * 5;
			Console.WriteLine($"num1 /= 2 é: {num1 /= 2}"); // num1 = num1 / 2;

			int a = 1;
			int b = a;
			Console.WriteLine($"Valores iniciais de a e b são: {a} {b}");
			//a++;
			Console.WriteLine($"Valor a++ é: {++a}");
			//b--;
			Console.WriteLine($"Valor b-- é: {--b}");

			// Atribuição por referência
			Console.WriteLine("Atribuição por referência");
			Console.WriteLine("-------------------------------");
			dynamic c = new System.Dynamic.ExpandoObject();
			c.nome = "João";
			Console.WriteLine($"dynamic c.nome = {c.nome}");

			dynamic d = c;
			d.nome = "Maria";
			Console.WriteLine($"dynamic d = c / d.nome = {d.nome}");
		}
	}
}
