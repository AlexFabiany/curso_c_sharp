﻿using System;

namespace CursoCSharp.Fundamentos {
	class InferenciaDeTipos {
		public static void Executar() {
			var nome = "Alex";
			// nome = 123; Não pode: a variável foi declarada, implicitamente, como string;
			Console.WriteLine("Nome: " + nome);

			int idade = 47;    // ou declara
			idade = 47;   // e depois inicializa
										//var idade = 32; ou declara inicializando; int idade = 32;
			Console.WriteLine("Idade: " + idade);

			int a;
			a = 25;

			int b = 38;
			Console.WriteLine($"a={a}, b={b} - O resultado de a+b é: {a + b}");
		}
	}
}
