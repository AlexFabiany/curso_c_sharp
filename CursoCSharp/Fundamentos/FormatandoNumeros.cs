﻿using System;
using System.Globalization;

namespace CursoCSharp.Fundamentos {
	class FormatandoNumeros {
		public static void Executar() {
			Console.Write("Digite uma valor: ");
			double valor = double.Parse(Console.ReadLine());

			Console.WriteLine($"Valor digitado formatado com decimais: {valor.ToString("F2")}"); // float, n casas decimais... ou #.##
			Console.WriteLine($"Valor digitado formatado para Currency: {valor.ToString("C2")}"); // currency, n casas decimais... ou R$ #.##
			Console.WriteLine($"Valor digitado formatado para porcentual: {valor.ToString("P2")}"); // porcentual, n casas decimais
			Console.WriteLine($"Valor digitado formatado para Currency: {valor.ToString("R$ ###,###.##")}");

			CultureInfo cultura = new CultureInfo("en-US"); // pt-BR
			Console.WriteLine($"Valor digitado formatado para Currency, independente da formatação do S.O. " +
				$"{valor.ToString("C2", cultura)}");

			int valorInt = (int)valor;// int.Parse(valor.ToString());
			Console.WriteLine($"Valor digitado convertido para int com tamanho 10: {valorInt.ToString("D10")}");
		}
	}
}
