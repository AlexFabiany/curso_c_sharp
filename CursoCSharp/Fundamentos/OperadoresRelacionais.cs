﻿using System;

namespace CursoCSharp.Fundamentos {
	class OperadoresRelacionais {
		public static void Executar() {
			//double nota = 6.0;
			double notaDeCorte = 7.0;
			Console.Write("Informe a nota: ");
			double.TryParse(Console.ReadLine(), out double nota);

			Console.WriteLine($"Nota: {nota}");
			Console.WriteLine($"Nota de corte: {notaDeCorte}");
			Console.WriteLine();
			Console.WriteLine($"Nota válida? {((nota >= 0) && (nota <= 10) ? "Sim" : "Não")}");
			Console.WriteLine($"Perfeito? {((nota == 10) ? "Sim" : "Não")}");
			Console.WriteLine($"Tem como melhorar? {((nota < 10) ? "Sim" : "Não")}");
			Console.WriteLine($"Passou por média? {((nota >= notaDeCorte) && (nota <= 10) ? "Sim" : "Não")}");
			Console.WriteLine($"Recuperação? {((nota < notaDeCorte) ? "Sim" : "Não")}");
			Console.WriteLine($"Reprovado? {((nota <= 3.0) ? "Sim" : "Não")}");
		}
	}
}
