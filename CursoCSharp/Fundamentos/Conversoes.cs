﻿using System;

namespace CursoCSharp.Fundamentos {
	class Conversoes {
		public static void Executar() {
			int inteiro = 10;
			double quebrado = inteiro;
			Console.WriteLine($"Inteiro: {inteiro} - Quebrado: {quebrado}");

			double nota = 9.7;
			int notaTruncada = (int)nota;
			Console.WriteLine($"Nota: {nota} - Nota truncada: {notaTruncada}");

			Console.Write("Digite sua idade: ");
			string idadeStr = Console.ReadLine();
			int idadeInt = int.Parse(idadeStr);
			Console.WriteLine("Idade convertida com int.Parse(): {0}", idadeInt);

			idadeInt = Convert.ToInt32(idadeStr);
			Console.WriteLine("Idade convertida com Convert.ToInt32(): {0}", idadeInt);

			Console.Write("Digite um número: ");
			string palavra = Console.ReadLine();
			int numero;
			int.TryParse(palavra, out numero);
			Console.WriteLine("Número digitado: {0}", numero);

			Console.Write("Digite outro número: ");
			int.TryParse(Console.ReadLine(), out int outroNumero);
			Console.WriteLine("Número digitado: {0}", outroNumero);
		}
	}
}
