﻿using System;

namespace Encapsulamento {
	public class SubCelebridade {
		//Todos acessam
		public string InfoPublica = "Tenho um instagram!";

		// Transmitido por herança
		protected string CorDoOlho = "Castanho";

		// Acessado dentro do mesmo projeto (assembly)
		internal ulong NumeroCelular = 5592999999999;

		// Transmitido por herança ou mesmo projeto (assembly)
		protected internal string JeitoDeFalar = "Uso muitas gírias!";

		// Mesma class ou herança no mesmo projeto (C# >= 7.2)
		private protected string SegredoDeFamilia = "Blá Blá Blá";

		// Padrão (private)
		bool UsaPhotohop = true;

		public void MeusAcessos() {
			Console.WriteLine("SubCelibridade...");
			Console.WriteLine(InfoPublica);
			Console.WriteLine(CorDoOlho);
			Console.WriteLine(NumeroCelular);
			Console.WriteLine(JeitoDeFalar);
			Console.WriteLine(SegredoDeFamilia);
			Console.WriteLine(UsaPhotohop);
		}
	}
}
